package com.monkeybros.ld29.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;

public class GameMap {
  TiledMap tileMap;
  public static final String INTERNAL_ID = "id";
  public static final float tileSize = 16.0f;
  private OrthographicCamera camera;
  private OrthogonalTiledMapRenderer renderer;
  private Vector3 initialCameraPosition;

  public GameMap() {
    tileMap = loadMap();
    camera = createCamera();
  }

  public TextureRegion getTextureRegion(MapId id) {
    for (TiledMapTile tile : tileMap.getTileSets().getTileSet(0)) {
      if (tile.getProperties().get(INTERNAL_ID) != null &&
          tile.getProperties().get(INTERNAL_ID).equals(id.name().toLowerCase())) {
        TextureRegion region = tile.getTextureRegion();
        return region;
      }
    }
    throw new IllegalArgumentException(String.format("no tile(id='%s')", id));
  }

  private TiledMap loadMap() {
    TiledMap map = new TmxMapLoader(new InternalFileHandleResolver()).load("world.tmx");
    renderer = new OrthogonalTiledMapRenderer(map, 1.0f / tileSize);
    return map;
  }

  private OrthographicCamera createCamera() {
    OrthographicCamera camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    camera.setToOrtho(false, Gdx.graphics.getWidth() / tileSize, Gdx.graphics.getHeight() / tileSize);
    camera.update();

    initialCameraPosition = camera.position.cpy();
    return camera;
  }

  public void render() {
    camera.update();
    renderer.setView(camera);
    renderer.render();
  }

  private void getBackgroundHeight(TiledMap map) {
    Integer backgroundHeight = getBackgroundLayer(map).getHeight();
  }

  private void getBackgroundWidth(TiledMap map) {
    Integer backgroundWidth = getBackgroundLayer(map).getWidth();
  }

  private TiledMapTileLayer getBackgroundLayer(TiledMap map) {
    return ((TiledMapTileLayer) map.getLayers().get("Background"));
  }

  public OrthographicCamera getCamera() {
    return camera;
  }

  public Vector3 getInitialCameraPosition() {
    return initialCameraPosition;
  }
}
