package com.monkeybros.ld29.map;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

class MapLocation {
  Vector3 viewportCoordinates = new Vector3();

  private int xTile;
  private int yTile;

  public MapLocation(Camera camera, int screenX, int screenY) {
    calculateTileCoordinates(camera, screenX, screenY);
  }

  public float getX() {
    return xTile;
  }

  public float getY() {
    return yTile;
  }

  public MapLocation calculateTileCoordinates(Camera camera, int screenX, int screenY) {
    camera.unproject(viewportCoordinates.set(screenX, screenY, 0));

    xTile = (int) Math.floor(viewportCoordinates.x);
    yTile = (int) Math.floor(viewportCoordinates.y);

    return this;
  }

  public Vector3 getViewportCoordinates() {
    return viewportCoordinates;
  }

  @Override
  public String toString() {
    return "MapLocation{" +
      "viewportCoordinates=" + viewportCoordinates +
      ", xTile=" + xTile +
      ", yTile=" + yTile +
      '}';
  }
}
