package com.monkeybros.ld29.map;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.monkeybros.ld29.events.EventManager;

import static java.lang.String.format;

public class MapInputProcessor implements InputProcessor {
  private final EventManager eventManager;
  private GameMap map;
  private MapLocation touchLocation;

  public MapInputProcessor(GameMap map, EventManager eventManager) {
    this.map = map;
    this.eventManager = eventManager;
  }

  @Override
  public boolean keyDown(int keycode) {
      return false;
  }

  @Override
  public boolean keyUp(int keycode) {
    return false;
  }

  @Override
  public boolean keyTyped(char character) {
    return false;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    touchLocation = new MapLocation(map.getCamera(), screenX, screenY);

    float xTile = touchLocation.getX();
    float yTile = touchLocation.getY();

    System.out.println(String.format("touched: %s, %s", xTile, yTile));

    eventManager.notifyHandlers(new CellSelectionEvent(xTile, yTile));

    return false;
  }

  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
    MapLocation mapLocation = new MapLocation(map.getCamera(), screenX, screenY);

    float dragDeltaX = mapLocation.getX() - touchLocation.getX();
    float dragDeltaY = mapLocation.getY() - touchLocation.getY();
  
    
    map.getCamera().position.x += dragDeltaX;
    if(map.getCamera().position.x < map.getCamera().viewportWidth / 2 )
    	map.getCamera().position.x = map.getCamera().viewportWidth / 2;
    if(map.getCamera().position.x  > -map.getCamera().viewportWidth / 2 + Integer.parseInt(map.tileMap.getProperties().get("width").toString())) 
    	map.getCamera().position.x  = -map.getCamera().viewportWidth  / 2 + Integer.parseInt(map.tileMap.getProperties().get("width").toString());

   	map.getCamera().position.y += dragDeltaY;
    if(map.getCamera().position.y < map.getCamera().viewportHeight/ 2 )
    	map.getCamera().position.y = map.getCamera().viewportHeight/ 2;
    if(map.getCamera().position.y  > -map.getCamera().viewportHeight / 2 + Integer.parseInt(map.tileMap.getProperties().get("height").toString())) 
    	map.getCamera().position.y  = -map.getCamera().viewportHeight  / 2 + Integer.parseInt(map.tileMap.getProperties().get("height").toString());

    
    touchLocation = mapLocation;

    System.out.println(format("position: %s", map.getCamera().position));

    System.out.println(format("drag: %s, %s, %s, %s", screenX, screenY, pointer, mapLocation));
    System.out.println(format("display: %s, %s", Gdx.graphics.getWidth(),Gdx.graphics.getHeight()));

    
    return true;
  }

  @Override
  public boolean mouseMoved(int screenX, int screenY) {
    return false;
  }

  @Override
  public boolean scrolled(int amount) {
    return false;
  }

}
