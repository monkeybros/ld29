package com.monkeybros.ld29.map;

import com.monkeybros.ld29.Game;
import com.monkeybros.ld29.events.GameEventHandler;

public class CellSelectionHandler extends GameEventHandler<CellSelectionEvent> {
  @Override
  public void handle(CellSelectionEvent event, Game game) {
    GameMap map = game.getGameMap();

    float offSetX = map.getCamera().position.cpy().sub(map.getInitialCameraPosition()).x;
    float offSetY = map.getCamera().position.cpy().sub(map.getInitialCameraPosition()).y;

    game.getSelectionMark().setX((event.getX() - offSetX) * GameMap.tileSize);
    game.getSelectionMark().setY((event.getY() - offSetY) * GameMap.tileSize);
  }
}
