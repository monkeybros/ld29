package com.monkeybros.ld29.map;

import com.monkeybros.ld29.events.GameEvent;

public class CellSelectionEvent extends GameEvent {
  float x;
  float y;

  public CellSelectionEvent(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public float getX() {
    return x;
  }

  public float getY() {
    return y;
  }
}
