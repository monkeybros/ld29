package com.monkeybros.ld29.menu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.monkeybros.ld29.events.EventManager;

public class StartMenu extends Menu {

	public StartMenu(EventManager eventManager,InputMultiplexer multiplexer,int x,int y,int width,int height) {
		super(eventManager,multiplexer,x,y,width,height);
	}

	@Override
	public void createElements() {
		Texture backgroundTexture = new Texture("menu.png");
		TextureRegion backgroundTextureRegion = new TextureRegion(backgroundTexture);
		TextureRegionDrawable backgroundDrawable = new TextureRegionDrawable(backgroundTextureRegion);

		menuTable.setFillParent(false);
		menuTable.setBackground(backgroundDrawable);
				
		createButton(menuTable, "Start","Start");
		menuTable.row().pad(10.0f);

		createButton(menuTable, "Help","Help");
		menuTable.row().pad(10.0f);

		createButton(menuTable, "Credits","Credits");
		menuTable.row().pad(10.0f);
		
		
		createButton(menuTable, "Quit","Quit");
		menuTable.row();
		
	}	
}
