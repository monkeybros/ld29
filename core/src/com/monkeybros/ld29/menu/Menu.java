package com.monkeybros.ld29.menu;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.monkeybros.ld29.SkinUtils;
import com.monkeybros.ld29.events.EventManager;

public abstract class Menu {
	  EventManager eventManager;

	  Stage menuStage;
	  Table menuTable;
		Array<Viewport> viewports;
		Array<String> names;
	  
	  public Menu(EventManager eventManager,InputMultiplexer multiplexer,int x,int y,int width,int height) {
		  this.eventManager = eventManager;
		  createMenu(multiplexer,x-width/2,y-height/2,width,height);
	  }
	  
	  private void createTable(int x,int y,int width,int height) {
		    menuTable = new Table();
		    menuTable.setPosition(x, y);
			menuTable.setSize(width, height);
		    menuStage.addActor(menuTable);
		  }	  
	  
	  protected abstract void createElements();
	
	  public void act(float delta) {
	  	menuStage.act(delta);
	  }
	  
	  public void draw() {
		  menuStage.draw();
	  }

	  
	  public void createMenu(InputMultiplexer multiplexer,int x,int y,int width,int height) {
		    menuStage = new Stage();
		    createTable(x,y,width,height);
		    createElements();		    
		    multiplexer.addProcessor(menuStage);
		  }
	  
	  
	  protected void createButton(Table table,String content, final String actionEventName) {
		    TextButton newButton = new TextButton(content, SkinUtils.getDefaultSkin());
		    table.add(newButton);

		    newButton.addListener(new InputListener() {
		      @Override
		      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		    	eventManager.notifyHandlers(new MenuEvent(actionEventName));
		    	//createDuelScene();
		        return true;
		      }
		    });
		  }	  
}
