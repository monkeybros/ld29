package com.monkeybros.ld29.menu;

import static java.lang.String.format;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.monkeybros.ld29.Game;
import com.monkeybros.ld29.events.EventManager;
import com.monkeybros.ld29.map.CellSelectionEvent;
import com.monkeybros.ld29.map.GameMap;

public class MenuInputProcessor implements InputProcessor {
	  private final EventManager eventManager;
	  private Game game;
	  
	  public MenuInputProcessor(Game game, EventManager eventManager) {
		    this.game = game;
		    this.eventManager = eventManager;
		  }	  
	  
	  @Override
	  public boolean keyDown(int keycode) {
	      if(keycode == Keys.ESCAPE)
	          game.showStartMenu();
	      return true;
	  }

	  @Override
	  public boolean keyUp(int keycode) {
	    return false;
	  }

	  @Override
	  public boolean keyTyped(char character) {
	    return false;
	  }

	  @Override
	  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		  return false;
	  }

	  @Override
	  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
	    return false;
	  }

	  @Override
	  public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;		  
	  }

	  @Override
	  public boolean mouseMoved(int screenX, int screenY) {
	    return false;
	  }

	  @Override
	  public boolean scrolled(int amount) {
	    return false;
	  }	  
	  
}
