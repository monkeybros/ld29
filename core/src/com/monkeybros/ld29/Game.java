package com.monkeybros.ld29;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.monkeybros.ld29.events.EventManager;
import com.monkeybros.ld29.events.GameEventHandler;
import com.monkeybros.ld29.map.CellSelectionEvent;
import com.monkeybros.ld29.map.CellSelectionHandler;
import com.monkeybros.ld29.map.GameMap;
import com.monkeybros.ld29.map.MapId;
import com.monkeybros.ld29.map.MapInputProcessor;
import com.monkeybros.ld29.menu.Menu;
import com.monkeybros.ld29.menu.MenuEvent;
import com.monkeybros.ld29.menu.MenuInputProcessor;
import com.monkeybros.ld29.menu.StartMenu;

public class Game extends ApplicationAdapter {
  EventManager eventManager;
  InputMultiplexer inputMultiplexer;
  List<Menu> menuList;
  
  GameMap gameMap;
  Tiles tiles;  
  
  SpriteBatch batch;
  Sprite selectionMark;

  Skin skin;  
 
  @Override
  public void create() {
	eventManager = new EventManager(this);
	gameMap = new GameMap();
	tiles = new Tiles();
	  
	
    inputMultiplexer = setupInput(gameMap, eventManager);
    batch = createSpriteBatch();

	menuList = new ArrayList<Menu>();
			    
    
	selectionMark = loadSelectionMark();

    addEventHandlers();
    selectionMark = loadSelectionMark();
    showStartMenu();
  }
  
  public void showStartMenu()
  {	  
		menuList.add(new StartMenu(eventManager,inputMultiplexer,Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2,200,250));  
  }
  
  private void clearMenuList() {
	  menuList.clear();
  }
  
  private void drawMenu() {
	  for(int i = 0;i < menuList.size();i++)
		  menuList.get(i).draw();
  } 
  
  private void addEventHandlers() {
	 eventManager.addHandler(MenuEvent.class, new GameEventHandler<MenuEvent>() {	  
	      @Override
	      public void handle(MenuEvent event, Game game) {
	    	  	if(event.eventName == "Quit") 
	    	  		Gdx.app.exit();	      
	    	  	if(event.eventName == "Start")
	    	  		game.clearMenuList();	 
	    	  }
	      });
	 eventManager.addHandler(CellSelectionEvent.class, new CellSelectionHandler());
  }

  private SpriteBatch createSpriteBatch() {
	SpriteBatch batch = new SpriteBatch();
	return batch;
  }  
  
  private InputMultiplexer setupInput(GameMap gameMap, EventManager eventManager) {
	InputMultiplexer inputMultiplexer = new InputMultiplexer();
	inputMultiplexer.addProcessor(new MapInputProcessor(gameMap, eventManager));
	inputMultiplexer.addProcessor(new MenuInputProcessor(this, eventManager));
	Gdx.input.setInputProcessor(inputMultiplexer);
	return inputMultiplexer;
  }
  
  
  public GameMap getGameMap() {
	return gameMap;
  }
	
  private Sprite loadSelectionMark() {
	  return tiles.createSprite(gameMap.getTextureRegion(MapId.MARK));
  }
  
  
  public Sprite getSelectionMark() {
	return selectionMark;
  }  

  @Override
  public void render() {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    gameMap.render();

    batch.begin();
    selectionMark.draw(batch);
    drawMenu();
    batch.end();
  }

}
