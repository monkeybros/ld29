package com.monkeybros.ld29;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Tiles {
  public static final String TILESET_FILE = "color_tileset_16x16_Jerom_Eiyeron_CC-BY-SA-3.0_8.png";

  private Texture tileSetTexture;

  public Tiles() {
    loadTilesTexture();
  }

  private void loadTilesTexture() {
    tileSetTexture = new Texture(TILESET_FILE);
  }

  public Sprite createSprite(TextureRegion region) {
    return new Sprite(tileSetTexture, region.getRegionX(), region.getRegionY(), region.getRegionWidth(), region.getRegionHeight());
  }
}
