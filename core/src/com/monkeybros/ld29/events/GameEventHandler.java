package com.monkeybros.ld29.events;

import com.monkeybros.ld29.Game;

public abstract class GameEventHandler<T extends GameEvent> {
	protected Game game;

	public abstract void handle(T event, Game game);
}
