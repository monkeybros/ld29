package com.monkeybros.ld29.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.monkeybros.ld29.Game;

public class EventManager {
	Game game;
	Map<Class<? extends GameEvent>, List<GameEventHandler<?>>> handlers = new HashMap<Class<? extends GameEvent>, List<GameEventHandler<?>>>();
	
	public EventManager(Game game) {
	  this.game = game;
	}
	
	public void notifyHandlers(GameEvent event) {
		for(GameEventHandler handler : getHandlersForType(event.getClass())) {
			handler.handle(event, game);
		}
	}
	
	public <T extends GameEvent> void addHandler(Class<T> eventClass, GameEventHandler<T> handler) {
		getHandlersForType(eventClass).add(handler);
	}
	
	List<GameEventHandler<?>> getHandlersForType(Class<? extends GameEvent> eventClass) {
		if (handlers.get(eventClass) == null) {
			handlers.put(eventClass, new ArrayList());
		}
		return handlers.get(eventClass);
	}
}
