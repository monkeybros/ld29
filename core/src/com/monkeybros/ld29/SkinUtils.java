package com.monkeybros.ld29;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class SkinUtils {
  public static final String SKIN_JSON = "uiskin.json";

  private static Skin skinInstance;

  public static Skin getDefaultSkin() {
    if (skinInstance == null) {
    	skinInstance =  new Skin(Gdx.files.internal(SKIN_JSON));
    }
    return skinInstance;
  }
}